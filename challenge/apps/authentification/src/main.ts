/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
import { NestFactory } from '@nestjs/core';

import { AuthModule } from './app/auth.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AuthService } from './app/auth.service';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AuthModule,
    {
      transport: Transport.TCP,
      options: {
        port: 2001
      }
    },
  );

  const globalPrefix = 'api';
  await app.listen();

  const seedService = app.get(AuthService);

  const isSeedCommand = process.argv[2];

  if (isSeedCommand === "seed") {
    seedService.seedDatabase();
  }
}

bootstrap();