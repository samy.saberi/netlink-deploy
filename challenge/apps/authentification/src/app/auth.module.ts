import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserEntity } from './user.entity';
import { ClientsModule, Transport, ClientProxyFactory } from '@nestjs/microservices';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { PassportModule } from '@nestjs/passport'; 
import { JwtStrategy } from './jwt-strategy';
import { UserLinkEntity } from './user-link.entity';
import { ConfigService } from './config/config.service';
import { TokenEntity } from './token.entity';

@Module({
  imports: 
  [ 
    TypeOrmModule.forRoot(
      {
        type: "postgres",
        host: process.env.DB_HOST,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        synchronize: true,
        logging: true,
        database: process.env.API_AUTH_DB_NAME,
        port: 5432,
        entities: [UserEntity, UserLinkEntity, TokenEntity]
      }
    ),
    TypeOrmModule.forFeature([UserEntity, UserLinkEntity, TokenEntity]),
    ClientsModule.register([
      {
        name: 'AUTHENTIFICATION',
        transport: Transport.TCP,
        options: {
          port: 2001,
        },
      },
    ]),
    PassportModule, 
    JwtModule.register(
      { secret: jwtConstants.secret, signOptions: { expiresIn: '60s' }}
    ),
  ],
  controllers: [AuthController],
  providers: [
    AuthService, 
    JwtStrategy,
    ConfigService,
    {
      provide: 'MAILER',
      useFactory: (configService: ConfigService) => {
        const mailerServiceOptions = configService.get('mailerService');
        return ClientProxyFactory.create(mailerServiceOptions);
      },
      inject: [ConfigService],
    },
  ],
})
export class AuthModule { }
