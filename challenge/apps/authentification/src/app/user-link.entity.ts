import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert
} from 'typeorm';

@Entity('authentification_user_link')
export class UserLinkEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'uuid'
    })
    user_id: string;

    @Column({
        type: 'text'
    })
    link: string;

    @Column({
        type: Boolean,
        nullable: false
    })
    is_used: boolean = false;

    @BeforeInsert()
    generateLink = () => {
        this.link = Math.random().toString(36).replace('0.', '');
    }
}