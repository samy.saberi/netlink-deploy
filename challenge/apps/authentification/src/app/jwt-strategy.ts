import { Injectable } from '@nestjs/common';
import {AuthService} from './auth.service';
import { AuthPayload } from './interfaces/auth-payload.interface';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { jwtConstants } from './constants'; 

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: jwtConstants.secret,
        });
    }

    // async validate(payload: AuthPayload) {
    //     return { userId: payload.userId, username: payload.email };
    // }
}
