import { Controller, UsePipes, UseFilters, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto, CreateUserFreelanceDto, LoginUserDto } from '@challenge/user';
import { RpcExceptionFilter } from './common/rpc-exception.filter';
import { CustomValidationPipe } from './common/CustomValidationPipe';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @EventPattern('create_user_client')
  @UsePipes(new CustomValidationPipe())
  @UseFilters(new RpcExceptionFilter())
  createUser(@Payload() createUserClientDTO : CreateUserClientDto){
    return this.authService.signupClient(createUserClientDTO);
  }

  @EventPattern('create_user_freelance')
  @UsePipes(new CustomValidationPipe())
  @UseFilters(new RpcExceptionFilter())
  createUserFreelance(@Payload() createUserFreelanceDto : CreateUserFreelanceDto){
    return this.authService.signupFreelance(createUserFreelanceDto);
  }
  
  @EventPattern('login_user')
  @UsePipes(new CustomValidationPipe())
  @UseFilters(new RpcExceptionFilter())
  loginUser(@Payload() LoginUserDto : LoginUserDto){
    return this.authService.login(LoginUserDto);
  }

  @EventPattern('user_confirm')
  confirmUser(confirmParams: {link: string;}) {
    return this.authService.confirmUser(confirmParams);
  }

  @EventPattern('token_decode')
  public async decodeToken(data: {
    token: string;
  }){
    let result = {
      status: HttpStatus.UNAUTHORIZED,
      message: 'Le token est incorrect ou n\'a pas été trouvé',
      item: null,
    };

    if (!data.token || !data.token.startsWith('Bearer')) {
      return result;
    }
    data.token = data.token.replace('Bearer ', '');

    const decodeTokenResult = await this.authService.decodeToken(data.token);

    if (decodeTokenResult.userId !== null) {
      result = {
        status: HttpStatus.OK,
        message: 'Le token a bien été décodé',
        item: decodeTokenResult,
      };
    }

    return result;
  }

  @EventPattern('token_destroy')
  public async destroyToken(data: {
    userId: string;
  }) {
    let result = {
      status: HttpStatus.BAD_REQUEST,
      message: 'Failed to destroy token',
    };

    try {
      await this.authService.deleteTokenForUserId(data.userId);
      result = {
        status: HttpStatus.OK,
        message: 'Le token a bien été détruit'
      };
    } catch (e) {}

    return result;
  }

  @EventPattern('get_user_by_id')
  public async getCardById(
    params
  ) {
    const user = await this.authService.getUserById(params);
    const result = {
      status: user ? HttpStatus.OK : HttpStatus.NOT_FOUND,
      message: user ? null : 'Cet utilisateur n\'a pas été trouvé',
      item: user,
    };

    return result;
  }

  // @UseGuards(AuthGuard)
  // @EventPattern('user_profile')
  // getProfile(confirmParams: {link: string;}) {
  //   return this.authService.me(confirmParams);
  // }
}
