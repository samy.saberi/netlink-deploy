import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('authentification_token')
export class TokenEntity {
    @PrimaryColumn('uuid')
    userId: string;

    @Column()
    token: string;
}