export interface AuthPayload {
    userId: string;
    firstName: string;
    lastName: string;
    email: string;
    roles: string[];
}