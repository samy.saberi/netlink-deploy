import { CSSProperties } from 'react';

export type UserCreds = {
    firstName: string,
    lastName: string,
    email: string;
    password: string;
};

export type CurrentUser = {
    id: string;
    createdOn: Date;
    email: string;
    token?: string;
};

export type AlertType = 'success' | 'info' | 'warning' | 'error' | undefined;

export type SnackBarAlert = {
    type: AlertType;
    msg: string;
};

export type HeaderStyle = CSSProperties;
export type RowStyle = CSSProperties;

export type Action = {
    type: string;
    payload: any;
};

export interface IAuth {
    currentUser: CurrentUser | null;
    err: any;
    isLoading: boolean;
}

