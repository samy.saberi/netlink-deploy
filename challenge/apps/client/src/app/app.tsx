// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import RegisterClient from './pages/auth/RegisterClient';
import styles from './app.module.scss';
import Freelance from './freelance/freelance';
import DetailFreelance from './freelance/detailFreelance';
import Home from './pages/Home';
import Navbar from './containers/Navbar';
import SearchResult from './pages/SearchResult';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import PaymentForm  from './pages/payment';

const stripePromise = loadStripe('pk_test_51NEBKpCy4jTzKN7NMwYiJx9k7ibALHKr3vQD0a0hZO9qz1gpwxPnqgOFJPFjq5W1FuBvyVt5syOKeFQpHcNZlDBI0000iuUqyQ');
import ConfirmationPage from './pages/auth/AccountConfirmation';
import { Route, Routes, Link } from 'react-router-dom';
import { createContext, useEffect, useState } from 'react';
import RegisterFreelance from './pages/auth/RegisterFreelance';
import jwt_decode from 'jwt-decode'
import { AuthContextProvider } from './providers/authProvider';

// interface User {
//   email: string,
//   sub: string
//   roles: string[],
//   username: string,
//   iat: string,
//   exp: number
// }

/* interface AuthContextProps {
  isAuthenticated: boolean;
  setIsAuthenticated: (value: boolean) => void;
  user: User | null
  setUser: (value: object) => void
}

export const AuthContext = createContext<AuthContextProps>({
  isAuthenticated: false,
  setIsAuthenticated: () => {},
  user: null,
  setUser: () => {}
}); */

export function App() {
  // const [isAuthenticated, setIsAuthenticated] = useState(false);
  // const [user, setUser] = useState({});
  useEffect(() => {
    const token = localStorage.getItem('jwt_token');
    if (token) {
      const decodedToken = jwt_decode(token);
      // setUser(decodedToken);
      console.log("DECODED TOKEN" + JSON.stringify(decodedToken))
      // setIsAuthenticated(true);
    }
  }, []);
  return (
    <div>
      {/* <AuthContext.Provider value={{ isAuthenticated, setIsAuthenticated, user, setUser }}> */}
      <AuthContextProvider>
        <Elements stripe={stripePromise}>

          <Navbar />
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/register-client" element={<RegisterClient />} />
            <Route path="/register-freelance" element={<RegisterFreelance />} />
            <Route path="/freelance" element={<Freelance />} />
            <Route path="/freelancer/:id" element={<DetailFreelance />} />
            <Route path="/confirm/:param" element={<ConfirmationPage />} />
            <Route path="/search" element={<SearchResult />} />
            <Route path="/payment/:id" element={<PaymentForm/>} />
            <Route path="/" element={<Home />} />
          </Routes>
        </Elements>
      </AuthContextProvider>

      {/* </AuthContext.Provider> */}
    </div>
  );
}
export default App;
