import styles from './freelance.module.scss';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Container, Grid, List, ListItem, ListItemText, Stack } from '@mui/material';
import { makeStyles } from '@material-ui/core';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import Footer from '../containers/Footer';

export interface FreelanceProps {}
export function DetailFreelance(props: FreelanceProps) {
  const [freelancer, setFreelancer] = useState();
  const [numberOfSkills, setNumberOfSkills] = useState(0);
  useEffect(() => {
    //get id from url
    const id = window.location.pathname.split('/')[2];
    axios.get('http://localhost:3000/api/getFreelancer/'+id)
      .then(response => {
        setFreelancer(response.data);
        setNumberOfSkills(Object.keys(response.data.skills).length);
        console.log('response.data', response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }, []);
  if (!freelancer) {
    return <div>Loading...</div>;
  }
  return (
    <div>
      <Container>
        <Grid container spacing={3} justifyContent="center" alignItems="stretch" >
          <Grid item xs={12}>
            <Grid container direction="row" justifyContent="flex-start" alignItems="stretch">
              <Grid item xs={12} sm={2}>
              (AVATAR)
              </Grid>
              <Grid item xs={12} sm={6}>
              <h2>{freelancer.lastName} {freelancer.firstName}</h2>
              {freelancer.job}<br/><br/>
              <LocationOnOutlinedIcon/> Localisé(e) à {freelancer.city}
              <br/>
              <br/>
              <Button variant="contained" sx={{textAlign: 'left'}} size="large">
                Tarif indicatif
                <br/><br/>
                {freelancer.hourlyRate}€/jour
              </Button>
              </Grid>
              <Grid item xs={12} sm={2}>
                <Button variant="contained">Proposer une mission</Button>
                <small> la mission ne démarrera que si le freelance accepte</small>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
      <hr/>
      <Container>
        <br />
        <Grid container spacing={3} justifyContent="flex-start" alignItems="stretch" >
          <Grid item xs={12} sm={4}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  Localisation et déplacement
                </Typography>
                <Typography variant="body2">
                  <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" >
                    <Grid item xs={1} sm={1}>
                      <LocationOnOutlinedIcon/>
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      <b>Localisation</b>
                    </Grid>
                  </Grid>
                  <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" >
                    <Grid item xs={1} sm={1}>
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      {freelancer.city}
                    </Grid>
                  </Grid>
                <br/>
                <br/>
                <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" >
                    <Grid item xs={1} sm={1}>
                      <LocationOnOutlinedIcon/>
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      <b>Télétravail</b>
                    </Grid>
                  </Grid>
                  <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" >
                    <Grid item xs={1} sm={1}>
                    </Grid>
                    <Grid item xs={10} sm={5}>
                      Effectue ses missions majoritairement à distance
                    </Grid>
                  </Grid>
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12} sm={8}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  Compétences ({numberOfSkills})
                </Typography>
                <Typography variant="body2">
                    <br/>
                    <Grid container direction="row" justifyContent="flex-start" alignItems="stretch">
                        {Object.keys(freelancer.skills).slice(0, 3).map(skillKey => (
                          <Grid key={skillKey} item xs={12} sm={6}>
                            <Button color="primary">{freelancer.skills[skillKey]}</Button>

                          </Grid>
                        ))}
                    </Grid>
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Grid container spacing={3} justifyContent="flex-start" alignItems="stretch" >
          <Grid item xs={12} sm={4}>
          </Grid>
          <Grid item xs={12} sm={8}>
            <Card sx={{ minWidth: 200 }}>
              <CardContent>
                <Typography variant="h5" component="div">
                  {freelancer.lastName} en quelques mots
                </Typography>
                <br/><br/>
                <Typography variant="body2">
                    {freelancer.description}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>

      <Footer />
    </div>
  )
}
export default DetailFreelance;
