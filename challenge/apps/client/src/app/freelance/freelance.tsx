import './freelance.scss';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Checkbox, FormControlLabel, Grid } from '@mui/material';
import DetailFreelance from './detailFreelance';
import Footer from '../containers/Footer';

/* eslint-disable-next-line */
export interface FreelanceProps { }

export function Freelance(props: FreelanceProps) {
  var cardStyle = {
    height: '20vw'
  }

  const [selectedFilter, setSelectedFilter] = useState("");

  const jwtToken = localStorage.getItem('jwt_token');

  const [freelancers, setFreelancers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    axios.get('http://samy-saberi.fr:3000/api/getFreelancers')
    .then(response => {
      setFreelancers(response.data);
      setLoading(false);
    })
    .catch(error => {
      setLoading(false);
      console.error('Erreur lors de la récupération des utilisateurs:', error);
    });
  }, []);

  const skillsOptions = ['php', 'javascript', 'symfony', 'html', 'css', 'nest', 'laravel'];

  const citiesFilter = [
    "paris",
    "montpellier",
    "lyon",
    "marseille",
    "toulouse",
    "nantes",
    "bordeaux",
    "lille",
    "rennes",
    "reims",
  ]

  const search = (keywords: string) =>{
    axios.get('http://localhost:3000/api/search', {
      params:{
        keywords: keywords
      }
    })
    .then(response => {
      setFreelancers(response.data);
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des utilisateurs:', error);
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault(); 
    search(selectedFilter);
  }

  const resetFilter = () => {
    setSelectedFilter("");
  }

  if (loading) {
    return <img src="./home/loading.gif" className="loading-results"/>;
  }

  return (
    <div>
      <div className="freelance-container">
        <div className="filter">
          <form className="" noValidate autoComplete="off">
            <h2>Filtrer par :</h2>
            <h3>Compétences :</h3>
            
            {skillsOptions.map((skill) => (
              <FormControlLabel
                  key={skill}
                  control={
                    <Checkbox
                      name="skills"
                      value={skill}
                      onChange={(e) => {
                        const { checked, value } = e.target;
                        if (checked) {
                          setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                        } else {
                          setSelectedFilter((prevFilter) =>
                            prevFilter
                              .split(' ')
                              .filter((keyword) => keyword !== value)
                              .join(' ')
                          );
                        }
                      }}
                    />
                  }
                  label={skill}
              />
            ))}

            <hr />

            <h3>Ville : </h3>
            {citiesFilter.map((city) => (
              <FormControlLabel
                  key={city}
                  control={
                    <Checkbox
                      name="skills"
                      value={city}
                      onChange={(e) => {
                        const { checked, value } = e.target;
                        if (checked) {
                          setSelectedFilter((prevFilter) => `${prevFilter} ${value}`);
                        } else {
                          setSelectedFilter((prevFilter) =>
                            prevFilter
                              .split(' ')
                              .filter((keyword) => keyword !== value)
                              .join(' ')
                          );
                        }
                      }}
                    />
                  }
                  label={city}
              />
            ))}
            <Button type="submit" variant="contained" className="reset-filter"  color="error" onClick={resetFilter}>Effacer</Button>
            <Button type="submit" variant="outlined" className="submit-filter" onClick={handleSubmit}>Filtrer</Button>
          </form>
        </div>          

        <Grid container spacing={2} sx={{ margin: 'auto', maxWidth: 1200 }}>
          {freelancers.length != 0 ?
          freelancers.map(freelancer => (
            <Grid item key={freelancer.id} xs={12} sm={6} md={4} lg={3}>
              <Card sx={{ minWidth: 200, minHeight: {xs: '250px', md: 'auto'} }} style={cardStyle}>
                <CardContent>
                  <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {freelancer.lastName} {freelancer.firstName}
                  </Typography>
                  <Typography variant="h5" component="div">
                    {freelancer.job}
                  </Typography>
                  <Typography sx={{ mb: 1.5 }} color="text.secondary">
                    {Object.keys(freelancer.skills).slice(0, 3).map(skillKey => (
                      <Button key={skillKey} color="primary" disabled>{freelancer.skills[skillKey]}</Button>
                    ))}
                  </Typography>
                  <Typography variant="body2">
                    {freelancer.description.length < 100 ? freelancer.description :
                      freelancer.description.slice(0, 100) + '...'
                    }
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" href={`freelancer/${freelancer.id}`}>Voir plus</Button>
                </CardActions>
              </Card>
            </Grid>
          ))
          :
            <>
              <h1 className="search-result-error-title">Aucun résultat pour cette recherche !</h1>

              <img src="./search-error.webp" className="search-result-error-img"/>
            </>
          }
        </Grid>
      </div>

      <Footer />
    </div>
  );
}

export default Freelance;
