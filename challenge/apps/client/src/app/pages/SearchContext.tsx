import React, { createContext, useState } from 'react';

interface SearchContextProps {
    searchData: any;
    setSearchData: (data: string) => void;
}

const defaultValue: SearchContextProps = {
    searchData: '',
    setSearchData: () => {},
};

export const SearchContext = React.createContext(defaultValue);

export const SearchProvider = ({ children }: React.PropsWithChildren<{}>) => {
    const [searchData, setSearchData] = useState('');
  
    return (
      <SearchContext.Provider value={{ searchData, setSearchData }}>
        {children}
      </SearchContext.Provider>
    );
};

  