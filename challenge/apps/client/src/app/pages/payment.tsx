import React, { useEffect, useState } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import axios from 'axios';
import AspectRatio from '@mui/joy/AspectRatio';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import Card from '@mui/joy/Card';
import CardContent from '@mui/joy/CardContent';
import Typography from '@mui/joy/Typography';
import Sheet from '@mui/joy/Sheet';
import { Grid } from '@mui/material';


const PaymentForm = () => {
  const stripe = useStripe();
  const elements = useElements();
  const [errorMessage, setErrorMessage] = useState('');
  const [quotation, setQuotation] = useState();
  useEffect(() => {
    const url = window.location.href;
    const id = url.substring(url.lastIndexOf('/') + 1);
    axios.get('http://localhost:3000/api/getQuotation/'+id)
      .then(response => {
        setQuotation(response.data.quotation);
        
        console.log('response.data', response.data.quotation);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }, []);
  
  if (!quotation) {
    return <>
    <h1 className="search-result-error-title">Oops, il se trouve que l'url n'existe pas !</h1>

    <img src="./search-error.webp" className="search-result-error-img"/>
</>;
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    const cardElement = elements.getElement(CardElement);

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: cardElement,
    });

    if (error) {
      console.log('[error]', error);
      setErrorMessage('Une erreur est survenue lors du paiement.');
    } else {
      try {
        console.log('[PaymentMethod]', paymentMethod);
        const url = window.location.href;
    const id = url.substring(url.lastIndexOf('/') + 1);
        const response = await axios.post('http://localhost:3000/api/payment/'+ id, {
          amount: 1000,
          currency: 'eur',
          payment_method: paymentMethod.id,
        });

        // La requête a réussi
        console.log('Paiement réussi !');
        //redirect to /success
        
        window.location.href = '/history-quotation';

                

      } catch (error) {
        console.log('Erreur lors de la demande de paiement.', error);
      }
    }
  };

  return (
    <div>
      <Grid container spacing={2} sx={{ margin: 'auto', maxWidth: 1200 }}>
      <Box
      sx={{
        width: '100%',
        position: 'relative',
      }}
    >
      <Box
        sx={{
          position: 'absolute',
          display: 'block',
          width: '1px',
          bgcolor: 'warning.300',
          left: '500px',
          top: '-24px',
          bottom: '-24px',
          '&::before': {
            top: '4px',
            display: 'block',
            position: 'absolute',
            right: '0.5rem',
            fontSize: 'sm',
            fontWeight: 'lg',
          },
          '&::after': {
            top: '4px',
            display: 'block',
            position: 'absolute',
            left: '0.5rem',
            fontSize: 'sm',
            fontWeight: 'lg',
          },
        }}
      />
      <Card
        orientation="horizontal"
        sx={{
          width: '100%',
          flexWrap: 'wrap',
          [`& > *`]: {
            '--stack-point': '500px',
            minWidth:
              'clamp(0px, (calc(var(--stack-point) - 2 * var(--Card-padding) - 2 * var(--variant-borderWidth, 0px)) + 1px - 100%) * 999, 100%)',
          },
          overflow: 'auto',
          resize: 'horizontal',
        }}
      >
        <AspectRatio ratio="1" maxHeight={182} sx={{ minWidth: 182, flex: 1 }}>
          <img
            src="https://images.unsplash.com/photo-1563013544-824ae1b704d3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80"
            srcSet="https://images.unsplash.com/photo-1563013544-824ae1b704d3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80"
            loading="lazy"
            alt=""
          />
        </AspectRatio>
        <CardContent>
          <Typography fontSize="xl" fontWeight="lg">
            Paiement du devis
          </Typography>
          <Typography fontSize="xl" fontWeight="lg">
            {quotation.amount/10} €
          </Typography>
          <Typography level="body2" fontWeight="lg" textColor="text.tertiary">
            Veuillez renseigner vos informations de paiement
          </Typography>
          <form onSubmit={handleSubmit}> 
            <Sheet sx={{bgcolor: 'background.level1',borderRadius: 'sm',p: 1.5,my: 1.5,display: 'flex',gap: 2,'& > div': { flex: 1 },}}>
              <div>
                  <CardElement/>
                  {errorMessage && <p>{errorMessage}</p>}
              </div>
            </Sheet>
            <Box sx={{ display: 'flex', gap: 1.5, '& > button': { flex: 1 } }}>
              <Button variant="solid" color="primary" type="submit" disabled={!stripe}>
                Payer
              </Button>
            </Box>
          </form>
        </CardContent>
      </Card>
    </Box>
    </Grid>
    </div>
  );
};

export default PaymentForm;
