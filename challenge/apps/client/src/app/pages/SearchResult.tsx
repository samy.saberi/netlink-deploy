import { useContext } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Grid} from '@mui/material';
import { SearchContext } from './SearchContext';
import '../styles/searchResult/index.css';

/* eslint-disable-next-line */
export interface FreelanceProps {}


export function SearchResult(props: FreelanceProps) {

    const { searchData } = useContext(SearchContext);

    if (typeof(searchData) === "string") {
        return <img src="./home/loading.gif" className="loading-results"/>;
    }

  return (
    <div>
        {searchData && searchData.length > 0 ? 
            <Grid container spacing={2} sx={{ margin: 'auto', maxWidth: 1200 }}>
            {searchData.map((freelancer: any) => (
            <Grid item key={freelancer.id} xs={12} sm={6} md={4} lg={3}>
                <Card sx={{ minWidth: 200 }}>
                <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {freelancer.lastName} {freelancer.firstName}
                    </Typography>
                    <Typography variant="h5" component="div">
                    {freelancer.job}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        {
                            freelancer.skills ? Object.keys(freelancer.skills).slice(0, 3).map(skillKey => (
                                <Button key={skillKey} color="primary" disabled>{freelancer.skills[skillKey]}</Button>
                            )) : ""
                        }
                    </Typography>
                    <Typography variant="body2">
                    {freelancer.description ? freelancer.description.length < 100 ? freelancer.description: freelancer.description.slice(0, 35) + '...'  : "" }
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" href={`freelancer/${freelancer.id}`}>Voir plus</Button>
                </CardActions>
                </Card>
            </Grid>
            ))}
            </Grid>

            :
            <>
                <h1 className="search-result-error-title">Aucun résultat pour cette recherche !</h1>

                <img src="./search-error.webp" className="search-result-error-img"/>
            </>

        } 


    </div>
  );
}

export default SearchResult;
