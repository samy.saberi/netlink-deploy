import React, { useState, ChangeEvent, FormEvent } from 'react';
import {
    TextField,
    Checkbox,
    FormControlLabel,
    Button,
    Container,
    Grid,
    Snackbar,
    Alert
} from '@mui/material';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

interface FormData {
    email: string;
    password: string;
    lastName: string;
    firstName: string;
    job: string;
    skills: string[];
    hourlyRate: number;
    city: string;
}

const skillsOptions = ['php', 'javascript', 'symfony', 'html', 'css', 'nest', 'laravel'];

const initialFormState: FormData = {
    email: '',
    password: '',
    lastName: '',
    firstName: '',
    job: '',
    skills: [],
    hourlyRate: 125,
    city: ''
};


const RegisterFreelance: React.FC = () => {
    const [formData, setFormData] = useState<FormData>(initialFormState);
    const [error, setError] = useState("");
    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (name === "hourlyRate") {
            setFormData((prevData) => ({
                ...prevData,
                [name]: parseInt(value, 10),
            }));
        } else {
            setFormData((prevData) => ({
                ...prevData,
                [name]: value,
            }));
        }
    };


    const handleCloseSnackbar = () => {
        setError('');
    };

    const navigate = useNavigate();

    const handleSkillsChange = (event: ChangeEvent<HTMLInputElement>) => {
        const { value, checked } = event.target;
        setFormData((prevData) => {
            if (checked) {
                return {
                    ...prevData,
                    skills: [...prevData.skills, value]
                };
            } else {
                return {
                    ...prevData,
                    skills: prevData.skills.filter((skill) => skill !== value)
                };
            }
        });
    };

    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();
        try {
            const response = await axios.post(`${import.meta.env.VITE_API_URL}/register-freelance`, formData);
            const data = response.data;
            if (data.status === 201) {
                navigate('/login', { state: { successMessage: data.message } });
            } else if (data.status === 500) {
                setError('Il y a eun un problème durant la création du compte');
            } else {
                setError(data.message)
            }
        }
        catch (error) {
            console.error;
        }
    };

    return (
        <Container maxWidth="lg" style={{ display: 'flex', alignItems: 'center', height: '100vh' }}>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            label="Profession"
                            name="job"
                            value={formData.job}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                        <TextField
                            label="Taux horaire"
                            name="hourlyRate"
                            type="number"
                            value={formData.hourlyRate}
                            onChange={handleChange}
                            required
                            fullWidth
                            style={{ marginBottom: '1rem' }}
                            inputProps={{
                                max: 5000,
                                min: 125,
                            }}
                        />

                        <TextField
                            label="Ville"
                            name="city"
                            value={formData.city}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                        <div style={{ marginBottom: '1rem' }}>
                            <p>Compétences :</p>
                            {skillsOptions.map((skill) => (
                                <FormControlLabel
                                    key={skill}
                                    control={
                                        <Checkbox
                                            name="skills"
                                            value={skill}
                                            checked={formData.skills.includes(skill)}
                                            onChange={handleSkillsChange}
                                        />
                                    }
                                    label={skill}
                                />
                            ))}
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            label="Nom"
                            name="lastName"
                            value={formData.lastName}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                        <TextField
                            label="Prénom"
                            name="firstName"
                            value={formData.firstName}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                        <TextField
                            label="Email"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                        <TextField
                            label="Mot de passe"
                            type="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                            fullWidth
                            required
                            style={{ marginBottom: '1rem' }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button type="submit" variant="contained" color="primary">
                            Inscription
                        </Button>
                    </Grid>
                </Grid>
            </form>

            <Snackbar open={!!error} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity='error'>
                    {error.includes('\n') ? (
                        error.split('\n').map((err, index) => (
                            <div key={index}>{err}</div>
                        ))
                    ) : (
                        <div>{error}</div>
                    )}

                </Alert>
            </Snackbar>
        </Container>

    );
};

export default RegisterFreelance;
