import { Link } from 'react-router-dom';

const Register = () => {
    return (
        <div
            style={{
                height: '100%',
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <div
                style={{
                    margin: '30px 0'
                }}
            >
                <h1>Quelle est votre situation ?</h1>
            </div>

            <div className="signup-identity-choice__cards">
                <Link
                    className="joy-choice-card"
                    to="/register-client"
                >

                    <img src="//dam.malt.com/signup/identity-client-v2" loading="lazy" alt="" />
                    <div className="joy-choice-card__content">
                        <div className="joy-choice-card__title">
                            Client</div>
                        <div className="joy-choice-card__subtitle">
                            Je recherche des freelances</div>
                    </div>
                </Link>

                <Link
                    className="joy-choice-card"
                    to="/register-freelance"
                >
                    <img src="//dam.malt.com/signup/identity-freelancer-v2" loading="lazy" alt="" />
                    <div className="joy-choice-card__content">
                        <div className="joy-choice-card__title">
                            Freelance</div>
                        <div className="joy-choice-card__subtitle">
                            Je crée mon profil</div>
                    </div>
                </Link>

                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <div>Déjà inscrit ?</div>
                    <div>
                        <Link
                            style={{ textDecoration: 'none', marginLeft: '0.5rem' }}
                            to="/login"
                        >
                            Se connecter
                        </Link>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default Register;
