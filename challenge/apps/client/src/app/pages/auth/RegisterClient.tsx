import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { TextField, Button, Container, Grid, Typography } from '@mui/material';
import { UserCreds } from '../../../types';
import axios from 'axios';
import { Snackbar, Alert } from '@mui/material';
import { Link } from 'react-router-dom';

const RegisterClient = () => {
    const [error, setError] = useState("");

    const [creds, setCreds] = useState<UserCreds>({
        firstName: '',
        lastName: '',
        email: '',
        password: ''
    });

    const handleCloseSnackbar = () => {
        setError('');
    };

    const navigate = useNavigate();

    const onChangeHandler = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { value, id } = e.target;

        setCreds((prevState) => ({
            ...prevState,
            [id]: value
        }));
    };

    const onSubmitHandler = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            const response = await axios.post(`${import.meta.env.VITE_API_URL}/register-client`, creds);
            const data = response.data;
            if (data.status === 201) {
                navigate('/login', { state: { successMessage: 'Votre compte a bien été créé. Vous devez le confirmer avec l\'email que vous allez recevoir' } });
            } else {
                setError(data.message);
            }
        }
        catch (error) {
            console.error;
        }
    };

    return (
        <Container maxWidth="sm">
            <form onSubmit={onSubmitHandler} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Typography variant="h4" align="center" gutterBottom>
                            Formulaire d'inscription
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            variant="outlined"
                            label="Nom"
                            onChange={onChangeHandler}
                            fullWidth
                            required
                            id='lastName'
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            variant="outlined"
                            label="Prénom"
                            onChange={onChangeHandler}
                            fullWidth
                            required
                            id='firstName'
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            label="Email"
                            onChange={onChangeHandler}
                            fullWidth
                            required
                            id='email'
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            label="Mot de passe"
                            type="password"
                            onChange={onChangeHandler}
                            fullWidth
                            required
                            id='password'
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                        >
                            Inscription
                        </Button>
                    </Grid>

                    <Grid item xs={12}>
                        <div style={{ display: 'flex', justifyContent: 'center'}}>
                            <div>
                                <Link
                                    style={{ textDecoration: 'none', marginLeft: '0.5rem', }}
                                    to="/login"
                                >
                                    Déjà un compte ?
                                </Link>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </form>

            <Snackbar open={!!error} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity='error'>
                    {error.includes('\n') ? (
                        error.split('\n').map((err, index) => (
                            <div key={index}>{err}</div>
                        ))
                    ) : (
                        <div>{error}</div>
                    )}

                </Alert>
            </Snackbar>
        </Container>
    );
};

export default RegisterClient;