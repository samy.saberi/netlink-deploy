import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import {
    Container,
    Typography,
    CircularProgress,
    Button
} from '@mui/material';

const ConfirmationPage = () => {
    const { param } = useParams();
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const [success, setSuccess] = useState(false);

    useEffect(() => {
        const confirmAccount = async () => {
            try {
                const response = await axios.get(`${import.meta.env.VITE_API_URL}/users/confirm/${param}`);
                if (response.data.status === 200) {
                    setSuccess(true);
                } else {
                    setError(response.data.message);
                }
            } catch (error) {
                setError('Une erreur s\'est produite lors de la confirmation du compte.');
            }
            setLoading(false);
        };

        confirmAccount();
    }, []);

    return (
        <Container maxWidth="sm" style={{ marginTop: '2rem' }}>
            {loading ? (
                <CircularProgress />
            ) : success ? (
                <div>
                    <Typography variant="h4" gutterBottom>
                        Compte confirmé avec succès!
                    </Typography>
                    <Typography variant="body1">
                        Votre compte a été confirmé avec succès. Vous pouvez maintenant vous connecter.
                    </Typography>
                    <Button variant="contained" color="primary" href="/login">
                        Se connecter
                    </Button>
                </div>
            ) : (
                <div>
                    <Typography variant="h4" gutterBottom>
                        Erreur lors de la confirmation du compte
                    </Typography>
                    <Typography variant="body1">
                        {error}
                    </Typography>
                    <Button variant="contained" color="primary" href="/register">
                        Retourner à l'inscription
                    </Button>
                </div>
            )}
            
        </Container>
    );
};

export default ConfirmationPage;
