import React, { useState, useEffect, useContext } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { Link, useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { Snackbar, Alert } from '@mui/material';
import axios from 'axios';
import { AuthContext } from '../../context/authContext';

const Login = () => {
    const useAuth = () => React.useContext(AuthContext);
    const { login } = useAuth();
    const [creds, setCreds] = useState({
        email: '',
        password: ''
    });

    const [error, setError] = useState('');

    const navigate = useNavigate();
    const { state } = useLocation();

    const handleCloseSnackbar = () => {
        setError('');
    };

    useEffect(() => {
        setCreds({
            email: '',
            password: ''
        });
    }, []);

    const onChangeHandler = (
        e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const { value, id } = e.target;

        setCreds((prevState) => ({
            ...prevState,
            [id]: value
        }));
    };

    const onSubmitHandler = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            const response = await axios.post(`${import.meta.env.VITE_API_URL}/login`, creds);
            const data = response.data;
            if (data.status === 201) {
                login(data.token);
                navigate('/', { state: { successMessage: data.message } });
            } else {
                setError(data.message);
            }
        }
        catch (error) {
            console.error;
        }
    };

    return (
        <div
            style={{
                height: '100%',
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column'
            }}
        >
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column'
                }}
            >
                <h1>Connexion</h1>
            </div>

            {state && state.successMessage && (
                <Snackbar open={true} autoHideDuration={6000}>
                    <Alert severity="success">{state.successMessage}</Alert>
                </Snackbar>
            )}

            <form
                onSubmit={onSubmitHandler}
                style={{ display: 'flex', flexDirection: 'column', width: '350px' }}
            >
                <TextField
                    style={{
                        margin: '0.5rem 0'
                    }}
                    variant="outlined"
                    id="email"
                    type="email"
                    onChange={onChangeHandler}
                    value={creds.email}
                    label="Email"
                    required
                />
                <TextField
                    style={{ margin: '0.5rem 0' }}
                    variant="outlined"
                    id="password"
                    type="password"
                    onChange={onChangeHandler}
                    value={creds.password}
                    label="Mot de passe"
                    required
                />
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    fullWidth
                >
                    Se connecter
                </Button>
            </form>

            <div style={{ display: 'flex', justifyContent: 'center', margin: '15px' }}>
                <div>
                    <Link
                        style={{ textDecoration: 'none', marginLeft: '0.5rem', }}
                        to="/register"
                    >
                        Pas encore inscrit ?
                    </Link>
                </div>
            </div>

            <Snackbar open={!!error} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity='error'>
                    {error.includes('\n') ? (
                        error.split('\n').map((err, index) => (
                            <div key={index}>{err}</div>
                        ))
                    ) : (
                        <div>{error}</div>
                    )}

                </Alert>
            </Snackbar>
        </div>
    );
};

export default Login;
