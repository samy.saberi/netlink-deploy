import React, { useContext, useEffect, useState } from 'react';
import { Box, Button, TextField } from '@mui/material';
import Footer from '../containers/Footer';
import axios from 'axios';
import { SearchContext } from './SearchContext';
import { useNavigate } from 'react-router-dom';
import '../styles/homePage/index.css';

const Home = () => {

  const { setSearchData } = useContext(SearchContext);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchLocation, setSearchLocation] = useState('');
  const [searchTermError, setSearchTermError] = React.useState(false);
  const [searchLocError, setSearchLocError] = React.useState(false);
  const navigate = useNavigate();

  const search = (keywords: string) =>{
    axios.get('http://localhost:3000/api/search', {
      params:{
        keywords: keywords
      }
    })
      .then(response => {
        setSearchData(response.data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
  }

  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (searchTerm.trim() === '' || searchTerm.length > 80){
      setSearchTermError(true); 
      return;
    }
    else if(searchLocation.length > 80){
      setSearchLocError(true);
      return;
    }
    setSearchTermError(false); 

    const keywords = encodeURIComponent(searchTerm).replace(/%20/g, ' ') + " " + encodeURIComponent(searchLocation).replace(/%20/g, ' ');
    search(keywords);
    navigate('/search');
  };

  return (
    <Box className="home">
      <img src="./home/geometry1.svg" className="home--geometry1"/>

      <Box className="home--introduction">
        <h1>Netlink, la plateforme qui vous met en  <strong>relation</strong> avec des Freelances pour propulser vos projets.</h1>
        <img src="./home/team.avif" className="home--teamImg"/>
      </Box>

      <Box className="home--search">
        <h1 className="home--search--title--mobile">Rechercher un freelance</h1>
        <form className="home--search--form" noValidate autoComplete="off" onSubmit={handleSubmit}>
          <TextField 
            id="outlined-basic" 
            label='Essayez "développeur front-end", "html", "java"' 
            variant="outlined" 
            className={searchTermError ? 'error' : ''}
            onChange={(event) => setSearchTerm(event.target.value)}
            required
          />
          <TextField 
            id="outlined-basic" 
            label="Lieu de la mission (ex: Paris, Lyon)" 
            variant="outlined" 
            className={searchLocError ? 'error' : ''} 
            onChange={(event) => setSearchLocation(event.target.value)} 
          />
          <Button type="submit" variant="contained" className="">Trouver un freelance</Button>
        </form>
      </Box>

      <Box className="home--community">
        <h1 className="home--community--title">Netlink c'est avant tout une communauté</h1>
        <p className="home--community--accroche">C'est aussi collaborer en toute simplicité</p>

        <Box className="home--community--icons">
          <Box className="home--community--icons--boxes">
            <img src="./home/handshake.png" className="home--community--icons--hands"/>
            <p className="home--community--icons--title">50K entreprises </p>
            <p className="home--community--icons-desc">À la recherche de freelances</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/rocket.png" className="home--community--icons--rocket"/>
            <p className="home--community--icons--title">+40K freelances</p>
            <p className="home--community--icons-desc">Aux multiples compétences</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/conversation.png" className="home--community--icons--conv"/>
            <p className="home--community--icons--title">1 solution dédiée</p>
            <p className="home--community--icons-desc">Pensée et conçue pour collaborer</p>
          </Box>
        </Box>
      </Box>

      <Footer />
    </Box>
  );
};

export default Home;
