import React from 'react';

export const authContext = {
    token: "",
    isAuthenticated: false,
    login: (token: string) => { },
    logout: () => { },
    user: {
        id: "",
        firstName: "",
        lastName: "",
        email: "",
        roles: [] as string[],
    }
}

export const AuthContext = React.createContext(authContext);
