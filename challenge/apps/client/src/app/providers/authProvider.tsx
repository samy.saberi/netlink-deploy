import React from "react";
import { AuthContext } from "../context/authContext";
import { DecodedTokenType, UserContextType, AuthContextProps } from "../types/auth";
import jwt_decode from "jwt-decode"
import { useLocalStorage } from "react-use";
import { useNavigate } from "react-router-dom";

export const AuthContextProvider = ({ children }: AuthContextProps) => {
    const navigate = useNavigate();
    const [tokenLs, setToken, removeToken] = useLocalStorage("jwt_token", "", { raw: true });
    const token = tokenLs || "";
    const defaultUser = React.useMemo(() => {
        if (tokenLs) {
            const decodedToken = jwt_decode<DecodedTokenType>(tokenLs);
            return {
                id: decodedToken.userId,
                firstName: decodedToken.firstName,
                lastName: decodedToken.lastName,
                email: decodedToken.email,
                roles: decodedToken.roles,
            }
        }
        return { id: "", firstName: "", lastName: "", email: "", roles: [] }
    }, [tokenLs])
    const [user, setUser] = React.useState<UserContextType>(defaultUser);

    const handleUpdateUser = React.useCallback((token: string) => {
        setToken(token);
        const decodedToken = jwt_decode<DecodedTokenType>(token);
        setUser({
            id: decodedToken.userId,
            firstName: decodedToken.firstName,
            lastName: decodedToken.lastName,
            email: decodedToken.email,
            roles: decodedToken.roles
        })
    }, [setUser])

    const logout = React.useCallback(() => {
        removeToken();
        setUser({ id: "", firstName: "", lastName: "", email: "", roles: [], })
        localStorage.clear();
        navigate("/");
    }, [setUser])

    const value = React.useMemo(() => ({
        token,
        isAuthenticated: user.id !== "",
        user,
        login: handleUpdateUser,
        logout
    }), [token, user, handleUpdateUser])

    return (
        <AuthContext.Provider value={value} >
            {children}
        </AuthContext.Provider>
    )
}