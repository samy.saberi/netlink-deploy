import React, { useState, useRef, useEffect, useContext } from 'react';
import { Button, Box, AppBar, Avatar, IconButton, Toolbar, Typography, TextField, Menu, MenuItem, ClickAwayListener, Grow, Paper } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import axios from 'axios';
import { SearchContext } from '../pages/SearchContext';
import { useNavigate } from 'react-router-dom';
import './navbar.css';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import { AuthContext } from '../context/authContext';

const Navbar = () => {
  const { isAuthenticated, logout } = useContext(AuthContext);

  const [isOpen, setIsOpen] = useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);
  const [open, setOpen] = useState(false);
  const { setSearchData } = useContext(SearchContext);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchLocation, setSearchLocation] = useState('');
  const [searchTermError, setSearchTermError] = React.useState(false);
  const [searchLocError, setSearchLocError] = React.useState(false);
  const navigate = useNavigate();

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const displaySearch = () => {
    setIsOpen(!isOpen);
  };

  const handleClose = (event: any) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleLogout = async () => {
    const token = localStorage.getItem('jwt_token')
    try {
      await axios.post(
        `${import.meta.env.VITE_API_URL}/logout`,
        null,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch (error) {
      console.error
    }
    logout();
  }

  const search = (keywords: string) =>{
    axios.get('http://localhost:3000/api/search', {
      params:{
        keywords: keywords
      }
    })
    .then(response => {
      setSearchData(response.data);
    })
    .catch(error => {
      console.error('Erreur lors de la récupération des utilisateurs:', error);
    });
  }

  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (searchTerm.trim() === '' || searchTerm.length > 80){
      setSearchTermError(true); 
      return;
    }
    else if(searchLocation.length > 80){
      setSearchLocError(true);
      return;
    }
    setSearchTermError(false); 

    const keywords = encodeURIComponent(searchTerm).replace(/%20/g, ' ') + " " + encodeURIComponent(searchLocation).replace(/%20/g, ' ');
    
    search(keywords);
    setIsOpen(false);
    navigate('/search');
  };

  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const closeModale = () => {
    setIsOpen(false);
  }

  return (
    <>
      <Box className="navbar">
        <Box flexGrow={1}>
          <AppBar position="fixed">
            <Toolbar>
              <Box className="flexMobileMenu">
                <Box className="menuMobile">
                  <div>
                    <IconButton
                      edge="start"
                      className="navbar-paper"
                      color="inherit"
                      aria-label="menu"
                      ref={anchorRef}
                      aria-controls={open ? 'menu-list-grow' : undefined}
                      aria-haspopup="true"
                      onClick={handleToggle}
                    >
                      <MenuIcon />
                    </IconButton>
                    <Menu
                      id="menu-list-grow"
                      anchorEl={anchorRef.current}
                      open={open}
                      onClose={handleClose}
                      TransitionComponent={Grow}
                      disablePortal
                    >
                      <ClickAwayListener onClickAway={handleClose}>
                        <Paper>
                          <MenuItem onClick={handleClose}>Explorer les profils</MenuItem>
                          {!isAuthenticated ? (
                            <>
                              <MenuItem onClick={handleClose}>Inscription</MenuItem>
                              <MenuItem onClick={handleClose}>Connexion</MenuItem>
                            </>
                          ) : (
                            <>
                              <MenuItem onClick={handleClose}>Mon profil</MenuItem>
                              <MenuItem onClick={handleLogout}>Se déconnecter</MenuItem>
                            </>
                          )}
                        </Paper>
                      </ClickAwayListener>
                    </Menu>
                  </div>
                </Box>

                <Box>
                  <Typography variant="h4" color="primary" className="mobileSiteName">
                    Netlink
                  </Typography>
                </Box>

                <Box>
                  <SearchIcon onClick={displaySearch} />
                </Box>
              </Box>

              <Box className="navbar-flexbox makeStyles-flexBox-4">
                <Avatar src="./netlink.png" className="navbar-logo" />
                <Button href="/freelance" style={{ color: 'black' }}>Explorer les profils</Button>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Button onClick={displaySearch} variant="outlined" className="desktopButtonSearch"><SearchIcon />Trouver un freelance</Button>
                {!isAuthenticated ? (
                  <>
                    <Button href="/register" variant="outlined" className="desktopButtonRegister">Inscription</Button>
                    <Button href="/login" style={{ textDecoration: 'underline', fontWeight: 'bold', color: 'black' }}>Connexion</Button>
                  </>
                ) : (
                  <>

                    <Button style={{ color: 'black' }} href="/profile">Profil</Button>

                    <Button onClick={handleLogout} sx={{ textDecoration: 'underline', fontWeight: 'bold', color: 'black' }}>
                      <ExitToAppIcon />
                    </Button>
                  </>
                )}
              </Box>

              <Box flexGrow={1} />
            </Toolbar>
          </AppBar>
        </Box>
        <Box className={`searchBox ${isOpen ? 'searchBoxOpen' : ''}`}>
          <Box className={`navbar-findFreelance makeStyles-findFreelance-7 ${isOpen ? 'findFreelanceOpen' : ''}`}>
            <Box className="findFreelance--title">
              <h1 style={{ textAlign: 'left', marginLeft: '20px' }}>Vous avez des projets, nous avons des experts.</h1>
            </Box>
            <form className="findFreelanceForm" noValidate autoComplete="off" onSubmit={handleSubmit}>
              <TextField 
                id="outlined-basic" 
                label='Essayez "développeur front-end", "html", "java"'
                variant="outlined" 
                className={searchTermError ? 'error' : 'findFreelanceForm--input'}
                onChange={(event) => setSearchTerm(event.target.value)}
                required
              />
              <TextField 
                id="outlined-basic" 
                label="Lieu de la mission (ex: Paris, Lyon)" 
                variant="outlined" 
                className={searchLocError ? 'error' : 'findFreelanceForm--input'}
                onChange={(event) => setSearchLocation(event.target.value)} 
              />
              <Button type="submit" variant="contained" className="findFreelanceForm--submit">Trouver un freelance</Button>
              <Button variant="contained" className="findFreelanceForm--close" onClick={closeModale}>Fermer</Button>
            </form>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Navbar;
