import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { CreateAssignmentDto } from '@challenge/assignment';
import { CreateQuotationDto } from '@challenge/quotation';
import { CreateUserFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('payment_create_user_client')
  createUser(@Payload() createUserDTO : CreateUserClientDto){
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('payment_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('payment_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  @EventPattern('payment_create_assignment')
  createAssignment(@Payload() createAssignmentDto: CreateAssignmentDto){
    return this.appService.createAssignment(createAssignmentDto);
  }

  @EventPattern('payment_create_quotation')
  createQuotation(@Payload() createQuotationDto: CreateQuotationDto){
    return this.appService.createQuotation(createQuotationDto);
  }

  @EventPattern('payment_stripe_create_payment')
  async createPayment(@Payload() body: {id: string, amount: number, currency: string, paymentId: string}) {
    const { id, amount, currency, paymentId} = body;    
    const paymentIntent = await this.appService.createPaymentIntent(id, amount, currency, paymentId);

    return { clientSecret: paymentIntent };
  }

 
  @EventPattern('payment_refund_payment_intent')
  async refundPaymentIntent(@Payload() body: { payment_intent: string}) { 
    const { payment_intent} = body;
    const refund = await this.appService.refundPaymentIntent(payment_intent);

    return { refund };
  }

  @EventPattern('payment_get_quotation')
  async getQuotation(@Payload() body: { id: string}) {
    const { id } = body;
    const quotation = await this.appService.getQuuotationById(id);

    return { quotation };
  }
}
