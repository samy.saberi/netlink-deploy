import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    BeforeInsert,
} from 'typeorm';

import { hash } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import { CreateUserClientDto } from '@challenge/user';

@Entity('payment_user')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'text',
        unique: true,
        nullable: false
    })
    email: string;

    @Column({
        type: 'text',
        nullable: false
    })
    password: string;

    @Column({
        type: String,
        length: 50,
        nullable: false
    })
    lastName: string;

    @Column({
        type: String,
        length: 50,
        nullable: false
    })
    firstName: string;

    @Column({
        type: String,
        length: 255,
        nullable: true
    })
    picture: string;

    @Column({
        type: 'boolean',
        nullable: true
    })
    status: boolean;

    @Column({
        type: 'json',
        nullable: true
    })
    skills: Array<string>;

    @Column({
        type: String,
        nullable: false,
        array: true
    })
    role: Array<string>;

    @Column({
        type: String,
        length: 50,
        nullable: true
    })
    city: string;

    @Column({
        type: 'date',
        nullable: true
    })
    birthDate: Date;

    @Column({
        type: 'integer',
        nullable: true
    })
    hourlyRate: number;

    @Column({
        type: String,
        length: 50,
        nullable: true
    })
    job: string;

    @Column({
        type: 'text',
        nullable: true
    })
    description: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column({
        type: Boolean,
        nullable: false
    })
    isConfirmed: boolean = false;

    @BeforeInsert()
    hashPassword = async () => {
        this.password = await hash(this.password, 8);
    };

    sanitizeObject = (options?: SanitizeUserOptions): CreateUserClientDto => {
        const { id, createdOn, email, token, lastName, firstName, role, password, isConfirmed } = this;
        const responseObj = { id, createdOn, email, lastName, firstName, role, password, isConfirmed };
        if (options?.withToken) {
            Object.assign(responseObj, { token });
        }
        return responseObj;
    };

    private get token() {
        const { id, email } = this;
        return sign(
            {
                id,
                email,
            },
            process.env.SECRET,
            { expiresIn: '3d' },
        );
    }
}

type SanitizeUserOptions = {
    withToken?: boolean;
};
