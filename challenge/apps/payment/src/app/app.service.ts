import { CreateUserClientDto } from '@challenge/user';
import { Injectable } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAssignmentDto } from '@challenge/assignment';
import { AssignmentEntity } from './assignment.entity';
import { CreateQuotationDto } from '@challenge/quotation';
import { QuotationEntity } from './quotation.entity';
import { SeedService } from './seed';
import { stripeConfig } from './stripe.config';
import { Stripe } from 'stripe';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }
  private readonly stripe: Stripe;

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,

    @InjectRepository(QuotationEntity)
    private quotationRepository: Repository<QuotationEntity>,
  ) {
    this.stripe = new Stripe(stripeConfig.apiSecret, {
      apiVersion: '2022-11-15',
    });
  }

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ['CLIENT'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  createAssignment(createAssignmentDto: CreateAssignmentDto){
    this.assignmentRepository.insert(createAssignmentDto);
    console.log("payment ", createAssignmentDto);
    return null;
  }

  createQuotation(createQuotationDto: CreateQuotationDto){
    this.quotationRepository.insert(createQuotationDto);
    console.log("payment", createQuotationDto);
    return null;
  }

  seedDatabase(){
    const obj = new SeedService(
      this.userRepository,
      this.assignmentRepository,
      this.quotationRepository
    );
    obj.seedDatabase();
  }

  async createPaymentIntent(id: string,amount: number, currency: string, paymentId: string) {
 console.log("aAAAA")
    const quotation = this.getQuuotationById(id);
    let amountQuotation = (await quotation).amount;
    amountQuotation = amountQuotation * 10;

    const paymentIntent = await this.stripe.paymentIntents.create({
      "amount" : amountQuotation,
      currency,
      payment_method_types: ['card'],
      payment_method: paymentId,
      confirm: true,
    });

    const assignmentId = this.getAssignmentById((await quotation).assignment.id);
    this.patchAssignment((await assignmentId).id, paymentIntent.id);
    return paymentIntent;
  }

  async getQuuotationById(id: string) {
    const quotation = await this.quotationRepository.findOne({where: {id: id}, relations: ["assignment"]});
    return quotation;
  }

  async getAssignmentById(id: string) {
    const assignment = await this.assignmentRepository.findOne({where: {id: id}});
    return assignment;
  }
    
  async patchAssignment(id: string, paymentIntent: string) {
    const assignment = await this.assignmentRepository.findOne({where: {id: id}});
    assignment.paymentIntent = paymentIntent;
    this.assignmentRepository.save(assignment);
    return assignment;
  }

  async refundPaymentIntent(paymentId: string) {

    const refund = await this.stripe.refunds.create({
      payment_intent: paymentId,
    });

    const assignment = await this.findAssignmentByPaymentIntent(paymentId);
    await this.patchAssignmentStatus(assignment.id);

    return refund;
  }

  async findAssignmentByPaymentIntent(paymentIntent: string) {
    const assignment = await this.assignmentRepository.findOne({where: {paymentIntent: paymentIntent}});
    return assignment;
  }

  async patchAssignmentStatus(id: string) {
    const assignment = await this.assignmentRepository.findOne({where: {id: id}});
    assignment.status = false;
    this.assignmentRepository.save(assignment);
    return assignment;
  }
}
