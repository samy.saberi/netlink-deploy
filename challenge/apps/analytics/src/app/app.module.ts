import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnalyticsEntity } from './analytics.entity';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: process.env.DB_HOST,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      synchronize: true,
      logging: true,
      database: process.env.API_ANALYTICS_DB_NAME,
      port: 5432,
      entities: [AnalyticsEntity]
    }
  )],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
