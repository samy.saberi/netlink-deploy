import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
  } from 'typeorm';
  
  
  @Entity('analytics_analytics')
  export class AnalyticsEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: String
    })
    tag: string;

    @Column({
        type: String
    })
    title: string;
}
  