import { CreateUserClientDto } from '@challenge/user';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { ReviewEntity } from './review.entity';
import { SeedService } from './seed';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';

@Injectable()
export class AppService {
  
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(ReviewEntity)
    private reviewRepository: Repository<ReviewEntity>,
  ) {}

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ["CLIENT"], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }
  
  seedDatabase(){
    const obj = new SeedService(
      this.userRepository, 
      this.reviewRepository, 
    );
    obj.seedDatabase();
  }

  
}
