import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { CreateUserFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('review_create_user_client')
  createUser(@Payload() createUserDTO : CreateUserClientDto){
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('review_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('review_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }
}
