import { UserEntity } from './user.entity';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne
} from 'typeorm';
  
  
    @Entity('review_review')
    export class ReviewEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'text',
    })
    description: string;


    @Column({
        type: 'integer',
    })
    rate: number;

    @ManyToOne(
        type => UserEntity,
        user => user.id,
    )
    ratedUser: UserEntity;

    @ManyToOne(
        type => UserEntity,
        user => user.id,
    )
    ratingUser: UserEntity;

}
  