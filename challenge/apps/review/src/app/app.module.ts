import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { ReviewEntity } from './review.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: "localhost",
      username: "postgres",
      password: "postgres",
      synchronize: true,
      logging: true,
      database: "review",
      port: 5432,
      entities: [UserEntity, ReviewEntity]
    }
  ),
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([ReviewEntity]),
  ClientsModule.register([
    {
      name: 'REVIEW',
      transport: Transport.TCP,
      options: {
        port: 2004,
      },
    },
  ]),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
