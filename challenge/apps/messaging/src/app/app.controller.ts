import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { CreateAssignmentDto } from '@challenge/assignment';
import { CreateQuotationDto } from '@challenge/quotation';
import { CreateUserFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('messaging_create_user_client')
  createUser(@Payload() createUserDTO : CreateUserClientDto){
    return this.appService.signup(createUserDTO);
  }

  @EventPattern('messaging_create_user_freelance')
  createUserFreelance(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    return this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('messaging_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  @EventPattern('messaging_create_assignment')
  createAssignment(@Payload() createAssignmentDto: CreateAssignmentDto){
    return this.appService.createAssignment(createAssignmentDto);
  }

  @EventPattern('messaging_create_quotation')
  createQuotation(@Payload() createQuotationDto: CreateQuotationDto){
    return this.appService.createQuotation(createQuotationDto);
  }
}
