import { AssignmentEntity } from './assignment.entity';
import { UserEntity } from './user.entity';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
  } from 'typeorm';
  
  
  @Entity('messaging_quotation')
  export class QuotationEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'integer',
    })
    amount: number;


    @Column({
        type: 'boolean',
        nullable: true,
    })
    status: boolean;

    @Column({
        type: 'text',
    })
    description: string;

    @Column({
        type: 'integer',
    })
    nbDays: number;
    

    @ManyToOne(
        type => UserEntity,
        user => user.id,
    )
    freelance: UserEntity;

    @ManyToOne(
        type => AssignmentEntity,
        assignment => assignment.id,
    )
    assignment: AssignmentEntity;
}
  