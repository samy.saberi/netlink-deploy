import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { AssignmentEntity } from './assignment.entity';
import { MessageEntity } from './message.entity';
import { QuotationEntity } from './quotation.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: process.env.DB_HOST,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      synchronize: true,
      logging: true,
      database: process.env.API_MESSAGING_DB_NAME,
      port: 5432,
      entities: [UserEntity, AssignmentEntity, MessageEntity, QuotationEntity]
    }
  ), 
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([AssignmentEntity]),
  TypeOrmModule.forFeature([QuotationEntity]),
  TypeOrmModule.forFeature([MessageEntity]),
  ClientsModule.register([
    {
      name: 'MESSAGING',
      transport: Transport.TCP,
      options: {
        port: 2002,
      },
    },
  ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
