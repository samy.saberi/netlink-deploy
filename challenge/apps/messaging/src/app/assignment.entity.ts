import { UserEntity } from './user.entity';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
  } from 'typeorm';
  
  
  @Entity('messaging_assignment')
  export class AssignmentEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'text',
        nullable: true
    })
    description: string;

    @Column({
        type: 'boolean',
        nullable: true
    })
    status: boolean;

    @Column({
        type: String,
        nullable: true
    })
    file: string;

    @Column({
        type: String,
        nullable: true
    })
    paymentIntent: string;

    @ManyToOne(
        type => UserEntity,
        user => user.id,
    )
    company: UserEntity;

    @ManyToOne(
        type => UserEntity,
        user => user.id,
    )
    freelance: UserEntity;
  
}
  