import { Injectable } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserClientDto } from '@challenge/user';
import { CreateAssignmentDto } from '@challenge/assignment';
import { AssignmentEntity } from './assignment.entity';
import { CreateQuotationDto } from '@challenge/quotation';
import { QuotationEntity } from './quotation.entity';
import { MessageEntity } from './message.entity';
import { SeedService } from './seed';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(AssignmentEntity)
    private assignmentRepository: Repository<AssignmentEntity>,

    @InjectRepository(QuotationEntity)
    private quotationRepository: Repository<QuotationEntity>,

    @InjectRepository(MessageEntity)
    private messageRepository: Repository<MessageEntity>,
  ) {}

  
  async signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ["CLIENT"], isConfirmed: false };
    return await this.userRepository.insert(user);
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ["FREELANCE"], isConfirmed: false  };
    return this.userRepository.insert(user);
  }

  createAssignment(createAssignmentDto: CreateAssignmentDto){
    this.assignmentRepository.insert(createAssignmentDto);
    console.log("messaging ", createAssignmentDto);
    return null;
  }

  createQuotation(createQuotationDto: CreateQuotationDto){
    this.quotationRepository.insert(createQuotationDto);
    console.log("message ", createQuotationDto);
    return null;
  }

  seedDatabase(){
    const obj = new SeedService(
      this.userRepository, 
      this.assignmentRepository, 
      this.quotationRepository, 
      this.messageRepository
    );
    obj.seedDatabase();
  }

}
