import {
    CanActivate,
    ExecutionContext,
    Injectable,
    Inject,
    HttpException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { HttpStatus } from '@nestjs/common/enums';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        @Inject('AUTHENTIFICATION') private readonly authentificationClient: ClientProxy,
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const secured = this.reflector.get<string[]>(
            'secured',
            context.getHandler(),
        );

        if (!secured) {
            return true;
        }

        const request = context.switchToHttp().getRequest();

        const userTokenInfo =
            await firstValueFrom(
                this.authentificationClient.send('token_decode', {
                    token: request.headers.authorization,
                }),
            );

        if (userTokenInfo.status !== HttpStatus.OK) {
            throw new HttpException(
                {
                    statusCode: userTokenInfo.status,
                    message: userTokenInfo.message,
                },
                userTokenInfo.status,
            );
        }

        const userInfo = await firstValueFrom(
            this.authentificationClient.send('get_user_by_id', {
                id: userTokenInfo.item.userId,
            }),
        );

        if (userInfo.status !== HttpStatus.OK) {
            throw new HttpException(
                {
                    statusCode: userInfo.status,
                    message: userInfo.message,
                },
                userInfo.status,
            );
        }

        request.user = userInfo.item;
        return true;
    }

}