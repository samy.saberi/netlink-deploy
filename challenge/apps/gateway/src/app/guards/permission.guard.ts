import {
    Injectable,
    Inject,
    CanActivate,
    ExecutionContext,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { Reflector } from '@nestjs/core';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class PermissionGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        @Inject('AUTHENTIFICATION') private readonly authentificationClient: ClientProxy,
    ) { }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const permission = this.reflector.get<{
            roles: string[];
            areAuthorized: boolean;
        }>('permission', context.getHandler());

        if (!permission) {
            return true;
        }

        const request = context.switchToHttp().getRequest();

        const userInfo = await firstValueFrom(
            this.authentificationClient.send('get_user_by_id', {
                id: request.user.id,
            }),
        );

        if (userInfo.status !== HttpStatus.OK) {
            throw new HttpException(
                'Impossible de vérifier les permissions de l\'utilisateur',
                HttpStatus.UNAUTHORIZED,
            );
        }

        if (permission.areAuthorized) {
            if (
                !userInfo.item.role.some((role) => permission.roles.includes(role))
            ) {
                throw new HttpException(
                    'Cet utilisateur n\'est pas autorisé à accéder à cette ressource',
                    HttpStatus.UNAUTHORIZED,
                );
            }
        } else {
            if (userInfo.item.role.some((role) => permission.roles.includes(role))) {
                throw new HttpException(
                    'Cet utilisateur n\'est pas autorisé à accéder à cette ressource',
                    HttpStatus.UNAUTHORIZED,
                );
            }
        }

        return true;
    }
}