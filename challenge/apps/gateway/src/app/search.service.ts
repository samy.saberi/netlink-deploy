import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class SearchService {

  constructor(
    @Inject('SEARCH') private readonly searchClient: ClientProxy,
  ){}

  async search(keywords: string){
    return await lastValueFrom(this.searchClient.send('search', keywords ));
  }

}
