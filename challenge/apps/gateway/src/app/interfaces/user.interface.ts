export interface IUser {
    id: string;
    email: string;
    roles: string[];
}

export enum IUserRoles {
    'FREELANCE',
    'CLIENT',
    'ADMIN'
}