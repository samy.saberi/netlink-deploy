import { CreateUserClientDto, LoginUserDto, ConfirmUserDto, CreateUserFreelanceDto } from '@challenge/user';
import { CreateAssignmentDto } from '@challenge/assignment';
import { Inject, Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom, firstValueFrom } from 'rxjs';
import { CreateQuotationDto } from '@challenge/quotation';

@Injectable()
export class AppService {

  constructor(
    @Inject('AUTHENTIFICATION') private readonly authentificationClient: ClientProxy,
    @Inject('MESSAGING') private readonly messagingClient: ClientProxy,
    @Inject('SEARCH') private readonly searchClient: ClientProxy,
    @Inject('REVIEW') private readonly reviewClient: ClientProxy,
    @Inject('PAYMENT') private readonly paymentClient: ClientProxy
  ){}

  async createUserClient(createUserClientDto: CreateUserClientDto) {
    try {
      const userClientCreateResponse = await lastValueFrom(this.authentificationClient.send('create_user_client', createUserClientDto));
      if (userClientCreateResponse.status !== HttpStatus.CREATED) {
        return new HttpException(userClientCreateResponse.message, userClientCreateResponse.status);
      } else if (userClientCreateResponse.status === HttpStatus.CREATED) {
        await lastValueFrom(this.messagingClient.send('messaging_create_user_client', createUserClientDto));
        await lastValueFrom(this.reviewClient.send('review_create_user_client', createUserClientDto));
        await lastValueFrom(this.paymentClient.send('payment_create_user_client', createUserClientDto));
        this.searchClient.send('search_create_user_client', createUserClientDto).subscribe();
        return userClientCreateResponse;
      }        
  
    } catch (e) {
      return new HttpException( e, 500 );
    }
  }

  async createUserFreelance(createUserFreelance: CreateUserFreelanceDto) {
    try {
      const userFreelanceCreateResponse = await lastValueFrom(this.authentificationClient.send('create_user_freelance', createUserFreelance));
      if (userFreelanceCreateResponse.status !== HttpStatus.CREATED) {
        return new HttpException(userFreelanceCreateResponse.message, userFreelanceCreateResponse.status);
      } else if (userFreelanceCreateResponse.status === HttpStatus.CREATED) {
        await lastValueFrom(this.messagingClient.send('messaging_create_user_freelance', createUserFreelance))
        await lastValueFrom(this.reviewClient.send('review_create_user_freelance', createUserFreelance));
        await lastValueFrom(this.paymentClient.send('payment_create_user_freelance', createUserFreelance));
        this.searchClient.send('search_create_user_freelance', createUserFreelance).subscribe();
        return userFreelanceCreateResponse;
      }    
    } catch (e) {
      return new HttpException( e, 500 );
    }

  }

  async confirmUser(params: ConfirmUserDto) {
    const confirmUserResponse = await lastValueFrom(this.authentificationClient.send('user_confirm', {link: params.link, }));
    if (confirmUserResponse.status!== HttpStatus.OK) {
        return new HttpException(confirmUserResponse.message, confirmUserResponse.status);
    } else if (confirmUserResponse.status === HttpStatus.OK) {
      await firstValueFrom(this.messagingClient.send('messaging_user_confirmed', {id: confirmUserResponse.userId}));
      await firstValueFrom(this.reviewClient.send('review_user_confirmed', {id: confirmUserResponse.userId }));
      await firstValueFrom(this.paymentClient.send('payment_user_confirmed', {id: confirmUserResponse.userId }));
      await firstValueFrom(this.searchClient.send('search_user_confirmed', {id: confirmUserResponse.userId }));
      return confirmUserResponse;
    }
  }

  async loginUser(loginUserDto: LoginUserDto) {
    return await lastValueFrom(this.authentificationClient.send('login_user', loginUserDto));
  }

  async logout(request) {
    const userInfo = request.user;

    return await firstValueFrom(
      this.authentificationClient.send('token_destroy', {
        userId: userInfo.id,
      }),
    );
  }

  async createAssigment(createAssignmentDto: CreateAssignmentDto){
    await lastValueFrom(this.messagingClient.send('messaging_create_assignment', createAssignmentDto));
    await lastValueFrom(this.paymentClient.send('payment_create_assignment', createAssignmentDto));
  }

  async createQuotation(createQuotationDto: CreateQuotationDto){
    await lastValueFrom(this.messagingClient.send('messaging_create_quotation', createQuotationDto));
    await lastValueFrom(this.paymentClient.send('payment_create_quotation', createQuotationDto));
  }

  //get all users
  async getUsersFromSeach(){
    return await lastValueFrom(this.searchClient.send('search_get_users', {}));
  }

  //get user by id
  async getUserFromSeach(id: string){
    return await lastValueFrom(this.searchClient.send('search_get_user', id));
  }

  //post payment
  async createPayment(id: string, amount: number, currency: string, paymentId: string) {    
    return await lastValueFrom(this.paymentClient.send('payment_stripe_create_payment', { id, amount, currency, paymentId }));
  }

  //refund payment
  async refundPayment(payment_intent: string) {
    return await lastValueFrom(this.paymentClient.send('payment_refund_payment_intent', { payment_intent }));
  }

  //get quotation by id
  async getQuotationById(id: string) {
    return await lastValueFrom(this.paymentClient.send('payment_get_quotation', { id }));
  }
}
