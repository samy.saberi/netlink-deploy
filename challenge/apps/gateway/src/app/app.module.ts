import { Module } from '@nestjs/common';
import { AuthGuard } from './guards/authorization.guard';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { APP_GUARD } from '@nestjs/core';
import { PermissionGuard } from './guards/permission.guard';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'AUTHENTIFICATION',
        transport: Transport.TCP,
        options: {
          port: 2001
        }
      }
    ]),
    ClientsModule.register([
      {
        name: 'MESSAGING',
        transport: Transport.TCP,
        options: {
          port: 2002
        }
      }
    ]),
    ClientsModule.register([
      {
        name: 'SEARCH',
        transport: Transport.TCP,
        options: {
          port: 2003,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'REVIEW',
        transport: Transport.TCP,
        options: {
          port: 2004,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'PAYMENT',
        transport: Transport.TCP,
        options: {
          port: 5000,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'MAILER',
        transport: Transport.TCP,
        options: {
          port: 2005,
        },
      },
    ]),
  ],
  controllers: [AppController, SearchController],
  providers: [
    AppService, 
    SearchService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: PermissionGuard,
    },
  ],
})
export class AppModule {}
