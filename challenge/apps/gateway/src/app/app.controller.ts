import { Body, Controller, Get, Param, Post, Req  } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateUserClientDto, LoginUserDto, ConfirmUserDto, ConfirmUserResponseDto, CreateUserFreelanceDto } from '@challenge/user';
import { CreateAssignmentDto } from '@challenge/assignment';
import { CreateQuotationDto } from '@challenge/quotation';
import { Authorization } from './decorators/authorization.decorator';
import { Permission } from './decorators/permission.decorator';
import { v4 as uuidv4 } from 'uuid';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) {}

  @Post("register-client")
  createUserEvent(@Body() createUserDto: CreateUserClientDto){
    const user = {
      ...createUserDto,
      id: uuidv4()
    };
    return this.appService.createUserClient(user);
  }

  @Post("register-freelance")
  createUserFreelanceEvent(@Body() createUserFreelanceDto: CreateUserFreelanceDto){
    const user = {
      ...createUserFreelanceDto,
      id: uuidv4()
    };
    return this.appService.createUserFreelance(user);
  }

  @Get('/users/confirm/:link')
  public async confirmUser(
    @Body() params: ConfirmUserDto,
  ): Promise<ConfirmUserResponseDto> {
    return this.appService.confirmUser(params);
  }

  @Post('login') 
  async login(@Body() loginUserDto: LoginUserDto) { 
    return this.appService.loginUser(loginUserDto); 
  }

  @Post('/logout')
  @Authorization(true)
  public async logout(@Req() request): Promise<void> {
    return this.appService.logout(request);

  }

  @Post("createAssignment")
  createAssignment(@Body() createAssignmentDto: CreateAssignmentDto){
    return this.appService.createAssigment(createAssignmentDto);
  }

  @Post("createQuotation")
  createQuotation(@Body() createQuotationDto: CreateQuotationDto){
    return this.appService.createQuotation(createQuotationDto);
  }

  @Get("getFreelancers")
  getUsersFromSeach(){
    return this.appService.getUsersFromSeach();
  }

// getFreelancer with id GET
  @Get("getFreelancer/:id")
  getUserFromSeach(@Param('id') id: string){
    return this.appService.getUserFromSeach(id);
  }

  //post payment
  @Post("payment/:id")
  createPayment(@Param('id') id: string, @Body() body: {amount: number, currency: string , payment_method: string}) {
    const {amount, currency, payment_method} = body;    
     console.log(id);
     
    return this.appService.createPayment(id, amount, currency, payment_method);
  }
 
  //post refund
  @Post("refund")
  refundPayment(@Body() body: { payment_intent: string }) {
    console.log(body);
    const { payment_intent } = body;
    return this.appService.refundPayment(payment_intent);
  }

  //get quotation by id
  @Get("getQuotation/:id")
  getQuotation(@Param('id') id: string) {
    return this.appService.getQuotationById(id);
  }

}
