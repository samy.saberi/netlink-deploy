/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppMailerModule } from './app/mailer.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppMailerModule,
    {
      transport: Transport.TCP,
      options: {
        port: 2005
      }
    },
  );
  await app.listen();

}

bootstrap();
