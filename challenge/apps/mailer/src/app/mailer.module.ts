import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { MailerConfigService } from './config/mailer-config.service';
import { MailerController } from './mailer.controller';
import { ConfigService } from './config/config.service';


@Module({
  imports: [
    MailerModule.forRootAsync({
      useClass: MailerConfigService,
    }),
  ],
  controllers: [MailerController],
  providers: [ConfigService],
})
export class AppMailerModule {}
