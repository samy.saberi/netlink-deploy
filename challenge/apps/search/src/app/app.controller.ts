import { Controller, Get, UseInterceptors} from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, Payload } from '@nestjs/microservices';
import { CreateUserClientDto } from '@challenge/user';
import { RemovePasswordInterceptor } from './interceptors/removePassword.interceptor';
import { CreateUserFreelanceDto } from '@challenge/user';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @EventPattern('search_create_user_client')
  handleUserCreated(@Payload() createUserDTO : CreateUserClientDto){
    this.appService.signup(createUserDTO);
  }

  @EventPattern('search_create_user_freelance')
  handleUserFreelanceCreated(@Payload() createUserFreelanceDTO : CreateUserFreelanceDto){
    this.appService.signupFreelance(createUserFreelanceDTO);
  }

  @EventPattern('search_user_confirmed')
  confirmUser(@Payload() id : string){
    return this.appService.confirmUser(id);
  }

  //get all users
  @EventPattern('search_get_users')
  @UseInterceptors(RemovePasswordInterceptor)
  handleGetUsers(){
    return this.appService.getFreelances();
  }

  //get user by id
  @EventPattern('search_get_user')
  @UseInterceptors(RemovePasswordInterceptor)
  handleGetUser(@Payload() id: string){
    return this.appService.getUser(id);
  }
}
