import { Injectable } from '@nestjs/common';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { faker } from '@faker-js/faker';
import { CategoryEntity } from './category.entity';
import * as bcrypt from 'bcrypt';


@Injectable()
export class SeedService {

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}

  async seedDatabase(){

    const skillsTab = [
      "php", 
      "javascript", 
      "laravel",
      "html",
      "css",
      "react",
      "nest",
      "symfony",
      "nodejs",
      "java",
      "go",
      "vuejs",
      "next",
      "c",
      "c++",
      "python",
      "threejs",
      "swift",
      "php",
      "sql",
      "c#",
      "objective-c",
      "rust",
      "ruby",
      "spring",
      "flask",
      "django",
      "express",
      "angular",
      "flutter",
      "rails"
    ]

    const role = [
      "ADMIN",
      "CLIENT",
      "FREELANCE"
    ]

    const jobs = [
      "front-end",
      "back-end",
      "fullstack",
      "devops",
      "mobile"
    ]

    const cities = [
      "paris",
      "montpellier",
      "lyon",
      "marseille",
      "annecy",
      "nice",
      "toulouse",
      "nantes",
      "strasbourg",
      "bordeaux",
      "lille",
      "rennes",
      "reims",
      "toulon",
      "grenoble",
      "angers",
      "quimper",
      "dijon",
      "villeurbanne",
      "brest",
      "tours",
      "amiens",
      "limoges",
      "boulogne",
      "châtillon",
      "metz",
      "orlean",
      "rouen",
      "argenteuil",
      "nancy",
      "caen",
      "mulhouse",
      "nanterre",
      "créteil",
      "poitiers",
      "avignon",
      "aubervilliers"
    ]

    for (let i=0; i<30; i++){

      // SEED TABLE USER

      const randomIndexJob = Math.floor(Math.random() * jobs.length);
      const randomIndexCities = Math.floor(Math.random() * cities.length);
      const salt = bcrypt.genSaltSync(10);
      const password = 'password';
      const hashedPassword = bcrypt.hashSync(password, salt);
  
      const firstName = faker.person.firstName();
      const lastName = faker.person.lastName();
      const email = faker.internet.email({firstName, lastName});
      const city = cities[randomIndexCities];
      const birthDate = faker.date.birthdate({min: 18, max: 65, mode: 'age'});
      const hourlyRate = faker.number.int({min: 20, max: 100});
      
      const job = jobs[randomIndexJob];
      const description = faker.lorem.paragraphs();
      const picture = faker.image.avatar();
      var skills = [];

      for (let i=0; i<5; i++){
        const randomIndexSkills = Math.floor(Math.random() * skillsTab.length);
        skills.push(skillsTab[randomIndexSkills]);
      }
      
      
      this.userRepository.insert([
        {
          email: email,
          password: hashedPassword,
          lastName: lastName,
          firstName: firstName,
          picture: picture,
          status: true,
          skills: skills,
          role: role,
          city: city,
          birthDate: birthDate,
          hourlyRate: hourlyRate,
          job: job,
          description: description,
          isConfirmed: false,
        },
      ])

      // SEED TABLE CATEGORY

      await this.categoryRepository.insert([
        {
          title: faker.word.noun()
        }
      ])

      // const randomFreelance = await this.userRepository
      // .createQueryBuilder()
      // .orderBy('RANDOM()')
      // .take(1)
      // .getOne();

      // const randomCategory = await this.categoryRepository
      // .createQueryBuilder()
      // .orderBy('RANDOM()')
      // .take(1)
      // .getOne();

      // SEED USER AS CATEGORIES
      

      // await this.userRepository.manager
      // .createQueryBuilder()
      // .insert()
      // .into('search_user_categories_search_category')
      // .values([{ searchUserId: randomFreelance, searchCategoryId: randomCategory }])
      // .execute();
      
    }  
  }
}
