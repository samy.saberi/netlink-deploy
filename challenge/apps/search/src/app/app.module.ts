import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryEntity } from './category.entity';
import { UserEntity } from './user.entity';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: "postgres",
      host: process.env.DB_HOST,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      synchronize: true,
      logging: true,
      database: process.env.API_SEARCH_DB_NAME,
      port: 5432,
      entities: [UserEntity, CategoryEntity]
    }
  ), 
  TypeOrmModule.forFeature([UserEntity]),
  TypeOrmModule.forFeature([CategoryEntity]),
  ClientsModule.register([
    {
      name: 'SEARCH',
      transport: Transport.TCP,
      options: {
        port: 2003,
      },
    },
  ]),
],
  controllers: [AppController, SearchController],
  providers: [AppService, SearchService],
})
export class AppModule {}
