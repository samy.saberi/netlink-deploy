import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';

@Injectable()
export class SearchService {

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async search(data: string){
   
    const skills = [ 
      "php", 
      "laravel",
      "html",
      "css",
      "react",
      "nest",
      "symfony",
      "nodejs",
      "java",
      "go",
      "vuejs",
      "next",
      "c++",
      "python",
      "threejs",
      "swift",
      "sql",
      "c#",
      "objective-c",
      "rust",
      "ruby",
      "spring",
      "flask",
      "django",
      "express",
      "angular",
      "flutter",
      "rails",
      "javascript"
    ];

    const cities = [
      "paris",
      "montpellier",
      "lyon",
      "marseille",
      "annecy",
      "nice",
      "toulouse",
      "nantes",
      "strasbourg",
      "bordeaux",
      "lille",
      "rennes",
      "reims",
      "toulon",
      "grenoble",
      "angers",
      "quimper",
      "dijon",
      "villeurbanne",
      "brest",
      "tours",
      "amiens",
      "limoges",
      "boulogne",
      "châtillon",
      "metz",
      "orlean",
      "rouen",
      "argenteuil",
      "nancy",
      "caen",
      "mulhouse",
      "nanterre",
      "créteil",
      "poitiers",
      "avignon",
      "aubervilliers"
    ];
    
    const jobs = [ 
      "front-end",
      "back-end",
      "fullstack",
      "devops",
      "mobile"
    ];

    var alreadyPast = false;
    var skillParamIndex = 0;
    var validKeyword = false;
    var cityParamIndex = 0;
    var alreadyPastCities = false;

    const keywordsArray = data.split(' ')
    .map(keyword => keyword.trim());

    const query = this.userRepository
    .createQueryBuilder('search_user');


    keywordsArray.forEach(keyword => {
        const lowercaseKeyword = keyword.toLowerCase();
        const skillParamName = `skill${skillParamIndex}`;

        const count = skills.filter((item, index, array) => {
          const lowercaseItem = item.toLowerCase();
          return (
            data.includes(lowercaseItem) &&
            !array.some((otherItem, otherIndex) =>
              otherIndex !== index && otherItem.toLowerCase().includes(lowercaseItem)
            )
          );
        }).length;
        
        if (skills.includes(lowercaseKeyword) && alreadyPast == false) {
            validKeyword = true;
            if(count > 1){
              query.andWhere('(EXISTS (SELECT 1 FROM json_array_elements_text(search_user.skills) AS skill WHERE skill = :'+ skillParamName +' LIMIT 1)');
            }else{
              query.andWhere('EXISTS (SELECT 1 FROM json_array_elements_text(search_user.skills) AS skill WHERE skill = :'+ skillParamName +' LIMIT 1)');
            }
          
          query.setParameter(skillParamName, lowercaseKeyword);

          skillParamIndex++;

          alreadyPast = true;

        }else if(skills.includes(lowercaseKeyword) && alreadyPast){
          validKeyword = true;
          if (skillParamIndex === count - 1) {
            query.orWhere('EXISTS (SELECT 1 FROM json_array_elements_text(search_user.skills) AS skill WHERE skill = :'+ skillParamName +' LIMIT 1))');
          }else{
            query.orWhere('EXISTS (SELECT 1 FROM json_array_elements_text(search_user.skills) AS skill WHERE skill = :'+ skillParamName +' LIMIT 1)');
          }
          query.setParameter(skillParamName, lowercaseKeyword);
          skillParamIndex++;
        }

        const cityParamName = `city${cityParamIndex}`;
      
        if (cities.includes(lowercaseKeyword) && alreadyPastCities == false) {
          validKeyword = true;
          query.andWhere('search_user.city ILIKE :'+cityParamName);
          query.setParameter(cityParamName, lowercaseKeyword);
          cityParamIndex++;
          alreadyPastCities = true;
        }else if(cities.includes(lowercaseKeyword) && alreadyPastCities){
          validKeyword = true;
          query.orWhere('search_user.city ILIKE :'+cityParamName);
          query.setParameter(cityParamName, lowercaseKeyword);
          cityParamIndex++;
        }

        if (jobs.includes(lowercaseKeyword)) {
          validKeyword = true;
            query.andWhere('search_user.job ILIKE :job', { job: lowercaseKeyword });
        }

    });

    var results = []

    if(validKeyword){
      results = await query.getMany();
    } 

    return results;
  }

}
