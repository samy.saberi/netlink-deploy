import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { CreateUserClientDto } from '@challenge/user';
import { SeedService } from './seed';
import { CategoryEntity } from './category.entity';
import * as bcrypt from 'bcrypt';
import { CreateUserFreelanceDto } from '@challenge/user';

@Injectable()
export class AppService {
  getData(): { message: string } {
    return { message: 'Hello API' };
  }

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(CategoryEntity)
    private categoryRepository: Repository<CategoryEntity>,
  ) {}

  signup(createUserDTO: CreateUserClientDto) {
    const hashedPassword = bcrypt.hashSync(createUserDTO.password, 10);
    const user = { ...createUserDTO, password: hashedPassword, role: ['CLIENT'], isConfirmed: false };
    return this.userRepository.insert(user);
  }


  signupFreelance(createUserFreelanceDTO: CreateUserFreelanceDto) {
    const hashedPassword = bcrypt.hashSync(createUserFreelanceDTO.password, 10);
    const user = { ...createUserFreelanceDTO, password: hashedPassword, role: ['FREELANCE'], isConfirmed: false };
    return this.userRepository.insert(user);
  }

  async confirmUser(id: string) {
    return await this.userRepository.update(id, { isConfirmed: true });
  }

  seedDatabase(){
    const obj = new SeedService(
      this.userRepository, 
      this.categoryRepository
    );
    obj.seedDatabase();
  }

  getUsers(){
    return this.userRepository.find();
  }

  getUser(id: string){
    return this.userRepository.findOne({where: {id: id}});
  }

  getFreelances(){
    return this.userRepository.find({where: {role: '{FREELANCE}'}});  }
}
