import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RemovePasswordInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(data => {
        if (Array.isArray(data)) {
          // Si la réponse est un tableau, par exemple lors de la récupération de plusieurs utilisateurs
          return data.map(item => this.removePassword(item));
        } else {
          // Sinon, supprimer simplement le champ password de l'objet de réponse
          return this.removePassword(data);
        }
      }),
    );
  }

  private removePassword(data: any): any {
    if (data && data.password) {
      // Supprimer le champ password de l'objet
      delete data.password;
    }
    return data;
  }
}
