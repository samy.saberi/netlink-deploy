export class CreateQuotationDto {
    amount?: number;
    status?: boolean;
    description?: string;
    nbDays?: number;
}