import { IsBoolean, IsDate, IsEmail, IsNotEmpty, IsOptional, IsString, IsArray, Length } from 'class-validator';

export class CreateUserClientDto {
    @IsString()
    readonly id: string;

    @IsEmail(undefined, {
        message: 'Veuillez fournir une adresse e-mail valide'
    })
    email: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(8, 10, { message: 'Le mot de passe doit comporter entre 8 et 10 caractères.' })
    password: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(2, 20, { message: 'Le nom de famille doit comporter entre 2 et 20 caractères.' })
    lastName: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(2, 20, { message: 'Le prénom doit comporter entre 2 et 20 caractères.' })
    firstName: string;

    @IsBoolean()
    isConfirmed: boolean = false;

    // @IsOptional()
    // @IsString()
    // picture?: string;

    // @IsBoolean()
    // @IsOptional()
    // status?: boolean;

    // @IsArray()
    // @IsOptional()
    // skills?: Array<string>;

    // @IsString()
    // @IsOptional()
    // city?: string;

    // @IsDate()
    // @IsOptional()
    // birthDate?: Date;

    // @IsOptional()
    // hourlyRate?: number;

    // @IsString()
    // @IsOptional()
    // job?: string;

    // @IsString()
    // @IsOptional()
    // description?: string;
}

export class LoginUserDto {
    @IsNotEmpty()
    @IsEmail()
    email?: string;

    @IsNotEmpty()
    @IsString()
    password?: string;
}