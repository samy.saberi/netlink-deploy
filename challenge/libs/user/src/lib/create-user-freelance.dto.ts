import { IsBoolean, IsDate, IsEmail, IsNotEmpty, IsOptional, IsString, IsArray, Length, IsInt, Min, Max } from 'class-validator';

export class CreateUserFreelanceDto {
    @IsString()
    readonly id: string;

    @IsEmail(undefined, {
        message: 'Veuillez fournir une adresse e-mail valide'
    })
    email: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(8, 10, { message: 'Le mot de passe doit comporter entre 8 et 10 caractères.' })
    password: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(2, 20, { message: 'Le nom de famille doit comporter entre 2 et 20 caractères.' })
    lastName: string;

    @IsString()
    @IsNotEmpty({ message: "Veuillez remplir tous les champs." })
    @Length(2, 20, { message: 'Le prénom doit comporter entre 2 et 20 caractères.' })
    firstName: string;

    @IsBoolean()
    isConfirmed: boolean = false;

    @IsString()
    @IsNotEmpty({ message: "Veuillez saisir votre métier" })
    @Length(5, 50, { message: 'Le métier doit comporter entre 5 et 50 caractères.' })
    job?: string;

    @IsArray()
    @IsNotEmpty({ message: "Veuillez sélectionner au moins une compétence" })
    skills: Array<string>;

    @IsInt({message: 'Veuillez saisir un nombre entier pour le taux horaire'})
    @IsNotEmpty({message: 'Veuillez saisir votre TJM'})
    @Min(125)
    @Max(5000)
    hourlyRate: number;

    @IsString()
    @IsNotEmpty({message: 'Veuillez saisir votre localisation'})
    @Length(5, 20, { message: 'La localisation doit comporter entre 5 et 20 caractères.' })
    city: string;

    // @IsOptional()
    // @IsString()
    // picture?: string;

    // @IsBoolean()
    // @IsOptional()
    // status?: boolean;

    // @IsDate()
    // @IsOptional()
    // birthDate?: Date;

    // @IsString()
    // @IsOptional()
    // description?: string;
}

