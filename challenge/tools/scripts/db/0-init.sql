-- Create default database used by the application
SELECT 'CREATE DATABASE authentification'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'authentification')\gexec

GRANT ALL PRIVILEGES ON DATABASE authentification TO postgres;

-- Create default database used by the application
SELECT 'CREATE DATABASE analytics'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'analytics')\gexec

GRANT ALL PRIVILEGES ON DATABASE analytics TO postgres;

-- Create default database used by the application
SELECT 'CREATE DATABASE messaging'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'messaging')\gexec

GRANT ALL PRIVILEGES ON DATABASE messaging TO postgres;

-- Create default database used by the application
SELECT 'CREATE DATABASE payment'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'payment')\gexec

GRANT ALL PRIVILEGES ON DATABASE payment TO postgres;

-- Create default database used by the application
SELECT 'CREATE DATABASE review'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'review')\gexec

GRANT ALL PRIVILEGES ON DATABASE review TO postgres;

-- Create default database used by the application
SELECT 'CREATE DATABASE search'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'search')\gexec

GRANT ALL PRIVILEGES ON DATABASE search TO postgres;

