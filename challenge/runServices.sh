#!/bin/bash

source ./.env.local


microservices=("gateway" "authentification" "messaging" "analytics" "review" "payment" "search" "mailer")

for service in "${microservices[@]}"
do
  (cd "./apps/$service" && nx serve) &
done

wait